package com.app.nonstopassignment.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.nonstopassignment.R;
import com.app.nonstopassignment.model.ResponseModel;
import com.app.nonstopassignment.model.SubPathDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class ListDataAdapter extends RecyclerView.Adapter<ListDataAdapter.ViewHolder> {
    private List<ResponseModel> mListArticles;
    private Context context;
    private static int mOldIndex = 0;

    public ListDataAdapter(Context context, List<ResponseModel> models) {
        this.mListArticles = new ArrayList<>();
        this.mListArticles.addAll(models);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layuot_path_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ResponseModel data = mListArticles.get(position);
        holder.mTextTitle.setText(data.getTitle());
        int scrolledPosition = 0;
        if (data.getSubPaths() != null) {
            holder.mTextPathCount.setText(data.getSubPaths().size() + " Paths");
            //set adapter for images
            PathImagesAdapter imagesAdapter = new PathImagesAdapter(context, data.getSubPaths());
            final LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.mRecyclerViewImages.setLayoutManager(manager);
            holder.mRecyclerViewImages.setAdapter(imagesAdapter);

            holder.mRecyclerViewImages.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    try {
                        holder.mRecyclerViewPathNames.smoothScrollToPosition(manager.findFirstVisibleItemPosition());
                        ((TextView) holder.mRecyclerViewPathNames.findViewHolderForAdapterPosition(manager.findFirstVisibleItemPosition()).itemView.findViewById(R.id.text_path_name)).setTypeface(Typeface.DEFAULT_BOLD);
                        ((TextView) holder.mRecyclerViewPathNames.findViewHolderForAdapterPosition(mOldIndex).itemView.findViewById(R.id.text_path_name)).setTypeface(Typeface.DEFAULT);
                        mOldIndex = manager.findFirstVisibleItemPosition();
                    } catch (Exception e) {

                    }
                }
            });

            //set adapter for names
            PathNamesAdapter pathNamesAdapter = new PathNamesAdapter(context, data.getSubPaths(), new PathNamesAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(SubPathDetailsModel item, int position) {

                    try {
                        ((TextView) holder.mRecyclerViewPathNames.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.text_path_name)).setTypeface(Typeface.DEFAULT_BOLD);
                        ((TextView) holder.mRecyclerViewPathNames.findViewHolderForAdapterPosition(mOldIndex).itemView.findViewById(R.id.text_path_name)).setTypeface(Typeface.DEFAULT);
                        mOldIndex = position;
                    } catch (Exception e) {
                        mOldIndex = position;
                    }

                    holder.mRecyclerViewImages.smoothScrollToPosition(position);
                    holder.mRecyclerViewPathNames.smoothScrollToPosition(position);

                }
            });
            holder.mRecyclerViewPathNames.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            holder.mRecyclerViewPathNames.setAdapter(pathNamesAdapter);
        } else {
            holder.mTextTitle.setText(data.getName());
            holder.mTextPathCount.setText("0 Paths");
        }

    }

    @Override
    public int getItemCount() {
        return mListArticles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextTitle, mTextPathCount;
        private LinearLayout mLayoutParent;
        private RecyclerView mRecyclerViewImages, mRecyclerViewPathNames;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextTitle = itemView.findViewById(R.id.text_title);
            mTextPathCount = itemView.findViewById(R.id.text_path_count);
            mLayoutParent = itemView.findViewById(R.id.layout_parent);
            mRecyclerViewImages = itemView.findViewById(R.id.rv_path_images);
            mRecyclerViewPathNames = itemView.findViewById(R.id.rv_path_names);
        }
    }
}