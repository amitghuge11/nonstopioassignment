package com.app.nonstopassignment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.nonstopassignment.R;
import com.app.nonstopassignment.model.SubPathDetailsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PathImagesAdapter extends RecyclerView.Adapter<PathImagesAdapter.ViewHolder> {
    private List<SubPathDetailsModel> mListArticles;
    private Context context;

    public PathImagesAdapter(Context context, List<SubPathDetailsModel> models) {
        this.mListArticles = new ArrayList<>();
        this.mListArticles.addAll(models);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layuot_path_image_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        SubPathDetailsModel data = mListArticles.get(position);
        if (data.getImage() != null) {
            Picasso.with(context)
                    .load(data.getImage())
                    .placeholder(R.mipmap.ic_launcher)
                    .into(holder.mImagePath);
        }
    }

    @Override
    public int getItemCount() {
        return mListArticles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImagePath;

        public ViewHolder(View itemView) {
            super(itemView);
            mImagePath = itemView.findViewById(R.id.image_path);
        }
    }
}