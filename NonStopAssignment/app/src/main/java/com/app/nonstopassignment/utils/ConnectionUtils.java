package com.app.nonstopassignment.utils;

import android.content.Context;
import android.widget.Toast;

import com.app.nonstopassignment.R;


public class ConnectionUtils {

    public static boolean isInternetAvailable(Context context) {
        ConnectionDetector cd = new ConnectionDetector(context);
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (!isInternetPresent) {
            Toast.makeText(context, context.getResources().getString(R.string.no_internet_connection),
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return isInternetPresent;
        }
    }
}
