package com.app.nonstopassignment.network;

import com.app.nonstopassignment.model.ResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NonStopIoServices {

    @GET("paths")
    Call<List<ResponseModel>> wsPaths();
}
