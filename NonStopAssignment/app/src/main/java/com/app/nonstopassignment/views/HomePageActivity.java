package com.app.nonstopassignment.views;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.widget.TextView;

import com.app.nonstopassignment.R;
import com.app.nonstopassignment.adapters.ListDataAdapter;
import com.app.nonstopassignment.model.ResponseModel;
import com.app.nonstopassignment.network.ApiHandler;
import com.app.nonstopassignment.network.NonStopIoServices;
import com.app.nonstopassignment.utils.ConnectionUtils;
import com.app.nonstopassignment.utils.ViewUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewPath;
    private TextView mTextUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        initializeViews();

        if (getIntent().getExtras() != null) {
            String name = "Hi," + getIntent().getExtras().getString("firstName") + " &#128075;";
            mTextUserName.setText(Html.fromHtml(name));
        }

        if (ConnectionUtils.isInternetAvailable(HomePageActivity.this)) {
            getPaths();
        }
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    private void initializeViews() {
        mTextUserName = findViewById(R.id.text_user_name);
        mRecyclerViewPath = findViewById(R.id.rv_path_list);
    }

    private void getPaths() {
        final ViewUtils utils = new ViewUtils();
        final ProgressDialog pd = utils.getProgressBar(HomePageActivity.this, "Loading...", "Please wait..!");
        NonStopIoServices apiService = ApiHandler.getApiService();
        final Call<List<ResponseModel>> loginCall = apiService.wsPaths();
        loginCall.enqueue(new Callback<List<ResponseModel>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<ResponseModel>> call,
                                   Response<List<ResponseModel>> response) {
                List<ResponseModel> responseModel = response.body();
                ListDataAdapter adapter = new ListDataAdapter(HomePageActivity.this, responseModel);
                mRecyclerViewPath.setLayoutManager(new LinearLayoutManager(HomePageActivity.this));
                mRecyclerViewPath.setAdapter(adapter);
                pd.hide();
            }

            @Override
            public void onFailure(Call<List<ResponseModel>> call,
                                  Throwable t) {
                pd.hide();
                utils.showMessage(HomePageActivity.this, getString(R.string.something_went_wrong));
            }
        });
    }
}
