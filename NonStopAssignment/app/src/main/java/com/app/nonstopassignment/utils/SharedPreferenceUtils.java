package com.app.nonstopassignment.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceUtils {
    static String PREFERENCE_NAME = "NonStopIoPref";
    static String USER_NAME = "user_name";

    public static void storeUserName(Context context, String userName) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return prefs.getString(USER_NAME, null);
    }
}
