package com.app.nonstopassignment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nonstopassignment.R;
import com.app.nonstopassignment.model.SubPathDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class PathNamesAdapter extends RecyclerView.Adapter<PathNamesAdapter.ViewHolder> {
    private List<SubPathDetailsModel> mListArticles;
    private Context context;
    private OnItemClickListener mListener;

    public PathNamesAdapter(Context context, List<SubPathDetailsModel> models, OnItemClickListener listener) {
        this.mListArticles = new ArrayList<>();
        this.mListArticles.addAll(models);
        this.context = context;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layuot_path_names_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        SubPathDetailsModel data = mListArticles.get(position);
        holder.bind(data, mListener, position);
        holder.mTextPathName.setText(data.getTitle());
        if (position == mListArticles.size() - 1) {
            holder.mImageRightArrow.setVisibility(View.GONE);
        } else {
            holder.mImageRightArrow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mListArticles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextPathName;
        private ImageView mImageRightArrow;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextPathName = itemView.findViewById(R.id.text_path_name);
            mImageRightArrow = itemView.findViewById(R.id.image_arrow);
        }

        public void bind(final SubPathDetailsModel item, final OnItemClickListener listener, final int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, position);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SubPathDetailsModel item, int position);
    }
}